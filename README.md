# Power Control

A cheesy way to power off the USB devices attached to a Raspberry Pi
when the printer's main power is switched off (via the PSU Plugin).
This all rather assumes you can turn the main printer power on and off
with a relay connected to a GPIO pin.

## Requirements

* [uhubctl](https://github.com/mvp/uhubctl) (a binary is checked in here, but
  you should probably compile your own)
* [PSU Plugin](https://github.com/kantlivelong/OctoPrint-PSUControl)
* PortLister Plugin (because it will auto-reconnect when the printer's port
  becomes available after power-up)
* `/etc/sudoers.d/010_pi-nopasswd` needs the following lines if passwordless
  sudo for all commands is not enabled:
  `pi ALL=(ALL) NOPASSWD: /opt/power_control/switch_on.sh`
  `pi ALL=(ALL) NOPASSWD: /opt/power_control/switch_off.sh`

## Setup

* Save out the files from this repo somewhere on your Octopi system
* Edit the `switch_on.sh` and `switch_off.sh` scripts to include the
  correct GPIO pin (note the prin used by the `gpio` binary, not the BOARD
  number!)
* Ensure that you can run both scripts without needing to enter your password.
  When you run the scripts, the printer and the USB devices should all power
  on/off.
* Configure the PSU Plugin to run a script to power up/down the printer
* Configure the PSU plugin to "sense" on the same pin as you used in the
  scripts above
* Configure the PSU plugin to disconnect the printer on power off
* Optionally turn the timeout down in PortLister (you can go as low as
  0 seconds) so that the printer is reconnected when it powers up

Now, hopefully if you click the "lightening bolt" icon in the top bar the
printer and USB devices should all power up. Clicking it again should
turn everything off.

