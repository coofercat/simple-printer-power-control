#!/bin/sh

GPIO_PIN="2" # board pin 13

if [ "${USER}" != "root" ]; then
	sudo -u root -- $0 $*
	exit $?
fi

/usr/bin/gpio mode ${GPIO_PIN} output
/usr/bin/gpio write ${GPIO_PIN} off
sleep 1
/opt/power_control/uhubctl -l 1-1 -a 0
sleep 1
/opt/power_control/uhubctl -l 1-1 -a 0
